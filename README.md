What this project is for
========================


 This repository contains my files (my code snippets and my synopses about the main book's topics) 
 for the book **"programming Python"** *4-th edition* by **Mark Lutz**.
 
 [here is the link to amazon](http://www.amazon.com/gp/product/0596158106/ref=pd_lpo_sbs_dp_ss_2?pf_rd_p=1944687502&pf_rd_s=lpo-top-stripe-1&pf_rd_t=201&pf_rd_i=0596009259&pf_rd_m=ATVPDKIKX0DER&pf_rd_r=0YS7R4NTPRB02B6GDR93) 




How to configure and install it 
===============================

 
 Simply copy (clone the repository) and see and read it.



 
An example of how to use it or get it running
=============================================

 
 `$ git clone https://gitlab.com/book_synopses_and_code_samples/programming_python_4_ed_m_lutz.git`



 
The license that the project is offered under
=============================================


 This project is offered under the MIT license




How to contribute to it 
=======================
 

 This project doesn't need to be contributed.

 But just in case you can always connect with me.